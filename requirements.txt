# --- Packages needed to run rettij --- #
-r requirements.prod.txt

# --- Packages needed to develop rettij --- #

# Test coverage
coverage~=5.5

# License checking
liccheck~=0.6.2

# Code documentation
Sphinx~=4.4.0
myst-parser~=0.17.0
sphinx-rtd-theme~=1.0.0

# Git pre-commit hook configuration
pre-commit~=2.14.0

# Packaging
setuptools~=57.4.0
wheel~=0.37.0

# Type stubs
types-setuptools~=57.0.2
types-PyYAML~=5.4.8
types-six~=1.16.1

# ----------------------------------------------------------------------------#
# The following packages are used and automatically installed by pre-commit
# You do not need to install them via requirements.
# To use the environment set up by pre-commit go to ~/.cache/pre-commit
# and search for and activate the virtual environment created by pre-commit
# ----------------------------------------------------------------------------#

# Code linting
# flake8

# Autoformatting
# black

# Type checking
# mypy