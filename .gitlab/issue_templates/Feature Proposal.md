## Feature Proposal
<!---
Please read this!

This template is meant for proposals to add NEW features to rettij.
For structural changes, please use the "Refactoring" template.

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "feature" label (https://gitlab.com/frihsb/rettij/-/issues?label_name%5B%5D=feature)
and verify the issue you're about to submit isn't a duplicate.
--->

### Problem to solve 

<!-- What is the user problem you are trying to solve with this issue? -->

### Proposal 

<!-- Use this section to explain the feature and how it will work. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->

/label ~feature