## Documentation

<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "documentation" label (https://gitlab.com/frihsb/rettij/-/issues?label_name%5B%5D=documentation)
and verify the issue you're about to submit isn't a duplicate.
--->

### What needs to be documented?

<!-- What needs to be documented and why? -->

### Documentation proposal

<!-- Use this section to explain how this may be documented. Add position, structure and contents for the documentation. -->

/label ~documentation