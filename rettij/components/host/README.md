This is the host component.
It is empty, because we do not really have programmatic control of you host system.

This directory needs to exist because topology parsing requires it.
Because we may at some point introduce hooks for the `host` device type, we do not make an exception for it.