import pathlib
import re

import setuptools
from pkg_resources import parse_requirements

with open("README.md", "r") as fh:
    long_description = fh.read()

    # Replace all relative links (not starting with '#' or 'http' with absolute GitLab links.
    # Solves broken links in PyPI (see #99)
    matches = re.findall(r"\[.*]\((.*)\)", long_description)
    for match in matches:
        assert isinstance(match, str)
        if not (match.startswith("#") or match.startswith("http")):
            long_description = long_description.replace(
                match, f"https://gitlab.com/frihsb/rettij/-/blob/master/{match}"
            )


with open("VERSION", "r") as fh:
    version = fh.read().strip("\n")

# https://stackoverflow.com/a/59971236
# Only works for simple requirements file (not nested, parameters etc.)
with pathlib.Path("requirements.prod.txt").open() as requirements_prod_txt:
    requirements = [str(requirement) for requirement in parse_requirements(requirements_prod_txt)]

setuptools.setup(
    name="rettij",
    version=version,
    author="Forschungsgruppe Rechnernetze und Informationssicherheit",
    author_email="fri@hs-bremen.de",
    description="rettij is an easy-to-use, scalable and co-simulation-capable ICT network simulator based on Kubernetes. It is designed for use with [Mosaik](https://mosaik.offis.de/) but can also be used standalone as a plain ICT network simulator.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://frihsb.gitlab.io/rettij/",
    project_urls={
        "Source Code": "https://gitlab.com/frihsb/rettij",
        "Documentation": "https://frihsb.gitlab.io/rettij",
        "Changelog": "https://gitlab.com/frihsb/rettij/-/releases",
        "Issue Tracker": "https://gitlab.com/frihsb/rettij/-/issues",
    },
    packages=setuptools.find_packages(),
    include_package_data=True,
    license="MIT",
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Information Technology",
        "Intended Audience :: Education",
    ],
    python_requires=">=3.8",
    test_suite="tests",
    install_requires=requirements,
    entry_points={
        "console_scripts": [
            "rettij=rettij:standalone",
        ],
    },
)
