FROM python:3.8-slim

COPY requirements.txt /
COPY requirements.prod.txt /

# Install git and clean update list
RUN apt-get update -yq && \
    apt-get install -yq git && \
    apt-get clean -yq && \
    rm -rf /var/lib/apt/lists/*

# Create and enable a venv as default method
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install -r requirements.txt
