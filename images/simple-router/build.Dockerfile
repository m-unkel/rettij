################################################################################################################
# FRR setup (based on https://github.com/FRRouting/frr/blob/master/doc/developer/building-frr-for-debian9.rst) #
################################################################################################################
FROM debian:buster-slim AS build

WORKDIR /

ENV DEBIAN_FRONTEND noninteractive

RUN set -x
RUN apt-get -yq update

# ipRoute2 is required for networking configuration.
# Telnet is required to use FRRs local vty.
# Strongswan is used for establishing a site-to-site IPSec VPN.
RUN apt-get install -yq iproute2 telnet strongswan

# Network and system utilities, mainly for debugging
RUN apt-get install -yq iputils-ping traceroute net-tools curl wget grep less tcpdump procps nano

# Python 3 runtime, required for FRR reconfiguration
RUN apt-get install -yq python3

# SNMP utilities required for FRR
RUN apt-get install -yq snmpd snmp

# General dependencies for building
RUN apt-get install -yq curl gnupg lsb-release git autoconf automake libtool make pkg-config bison flex build-essential texinfo doxygen

#############################
# Build and install netsnmp #
#############################
# Netsnmp contains AgentX, which is the only SNMP agent supporte by FRR
# Netsnmp does not have installable packages available

# dependencies for net-snmp build
RUN apt-get install -yq perl libnet-snmp-perl libsnmp-perl libperl-dev

# install net-snmp with –with-mib-modules=agentx flag, needed to compile FRR with --snmp-enabled
RUN curl -L -o net-snmp-5.9.tar.gz https://sourceforge.net/projects/net-snmp/files/net-snmp/5.9/net-snmp-5.9.tar.gz/download
RUN tar -xvf net-snmp-5.9.tar.gz
WORKDIR /net-snmp-5.9/
RUN ./configure \
    --with-mib-modules=agentx \
    --with-default-snmp-version=3 \
    --with-logfile=/var/log/snmpd.log \
    --with-persistent-directory=/var/net-snmp \
    --enable-static \
    --enable-ipv6 \
    --with-pic \
    --with-cc=gcc CC=gcc CFLAGS=-m64
RUN make
RUN make install

WORKDIR /

#############################
# Build and install libyang #
#############################
# FRR requires libyang >= 1.0.184, Debian Buster only has 0.16 available, so we have to build it ourselves

# Dependencies for libyang (https://github.com/CESNET/libyang/blob/master/README.md#build-requirements)
RUN apt-get install -yq libpcre3-dev cmake

## install libyang from source
RUN git clone https://github.com/CESNET/libyang.git --depth 1
RUN mkdir -p libyang/build
WORKDIR /libyang/build
RUN cmake \
    -D ENABLE_LYD_PRIV=ON \
    -D CMAKE_INSTALL_PREFIX:PATH=/usr \
    -D CMAKE_BUILD_TYPE:String="Release" ..
RUN make
RUN make install

WORKDIR /

#################################
# Build and install FRR routing #
#################################
# FRR needs to be built from source to enable SNMP on Debian

# Dependencies for building FRR
RUN apt-get install -yq \
    libreadline-dev libjson-c-dev libc-ares-dev python3-dev python3-pytest \
    python3-sphinx libsnmp-dev libsystemd-dev libcap-dev libcmocka-dev

# Download FRR source, configure and compile it
RUN git clone https://github.com/frrouting/frr.git frr --depth 1
WORKDIR /frr/
RUN ./bootstrap.sh \
    && ./configure \
        --disable-doc \
        --localstatedir=/var/opt/frr \
        --sbindir=/usr/local/sbin/frr \
        --sysconfdir=/etc/frr \
        --enable-multipath=64 \
        --enable-user=frr \
        --enable-group=frr \
        --enable-vty-group=frrvty \
        --enable-configfile-mask=0640 \
        --enable-logfile-mask=0640 \
        --enable-fpm \
        --enable-snmp \
        --with-pkg-extra-version=-FRI-Container
RUN make
RUN make check
RUN make install

#####################################
# Clean up build files and packages #
#####################################
# Do not remove the '-dev' packages, unless FRR ist built statically

WORKDIR /
RUN apt-get purge -y --autoremove \
    lsb-release git autoconf automake libtool make pkg-config bison flex build-essential texinfo doxygen \
    python3-dev python3-pytest python3-sphinx cmake \
    && apt-get clean -y
RUN rm -rf /net-snmp-5.9.tar.gz \
    && rm -rf /net-snmp-5.9 \
    && rm -rf /libyang \
    && rm -rf /frr

######################################
# Set up FRR and SNMP configurations #
######################################

# Add FRR groups and user
RUN addgroup --system --gid 92 frr \
    && addgroup --system --gid 85 frrvty \
    && adduser --system --ingroup frr --home /var/opt/frr/ --gecos "FRR suite" --shell /bin/false frr \
    && usermod -a -G frrvty frr

# Create FRR directories
# /var/log/frr -> FRR logging
# /var/opt/frr -> PID and VTY files
# /var/run/frr -> Temporary files for reconfiguration
# /etc/frr -> Config files
RUN install -m 755 -o frr -g frr -d /var/log/frr \
    && install -m 755 -o frr -g frr -d /var/opt/frr \
    && install -m 755 -o frr -g frr -d /var/run/frr \
    && install -m 775 -o frr -g frrvty -d /etc/frr

# Create empty FRR configuration files
RUN install -m 640 -o frr -g frr /dev/null /etc/frr/bgpd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/ospfd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/ospf6d.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/isisd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/ripd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/ripngd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/pimd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/ldpd.conf \
    && install -m 640 -o frr -g frr /dev/null /etc/frr/nhrpd.conf \
    && install -m 640 -o frr -g frrvty /dev/null /etc/frr/vtysh.conf

# Setup predefined FRR configuration files
COPY --chown=frr:frr frr/daemons /etc/frr/daemons
RUN chmod 640 /etc/frr/daemons
COPY --chown=frr:frr frr/zebra.conf /etc/frr/zebra.conf
RUN chmod 640 /etc/frr/zebra.conf

# Setup SNMP configurations
RUN mkdir -p /etc/snmp/
COPY --chown=frr:frr snmp/snmpd.conf /etc/snmp/snmpd.conf
RUN chmod 444 /etc/snmp/snmpd.conf
COPY --chown=frr:frr snmp/zebra.conf /etc/snmp/zebra.conf
RUN chmod 444 /etc/snmp/zebra.conf

# create python symlink for frr executables that rely on /usr/bin/python as an interpreter
RUN ln -s /usr/bin/python3 /usr/bin/python

# include shared libs
RUN echo include /usr/local/lib/frr >> /etc/ld.so.conf \
    && ldconfig

# Set VTY password (required for terminal access)
RUN echo "password rettij" > /etc/frr/frr.conf

##################################################

# Start snmpd service & FRR and go into endless loop. Container will exit with code 0 if only frr is started.
CMD ["bash", "-c", "service snmpd start && /usr/local/sbin/frr/frrinit.sh start && tail -f /dev/null"]
