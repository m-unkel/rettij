# FRR routing Docker image

This is a containerised version of the FRR routing software.

Daemons for the different routing protocols can be activated and configured via the `frr/daemons` file.
The following daemons are activated by default:
- bgpd
- ospfd
- ripd


## Building

### Using the pre-built packages
Installing `frr`, `frr-pythontools` and `frr-snmp` along with `snmpd` and `snmp` seems to work fine.

This is done in the default `Dockerfile`.

The build requires `Docker Engine` version ` 20.10` or higher, as well as the use of [BuildKit](https://docs.docker.com/engine/reference/builder/#buildkit). You can check your Docker version using `docker version`.

```bash
export DOCKER_BUILDKIT=1
docker build -t frihsb/rettij_simple-router:latest .
```

Currently, using the pre-built packages fulfills all of our requirements.
It may however become necessary, to build FRR from source yourself.

### Building FRR manually
Building FRR into a container comes with a few complications:

1.  FRR and some of its dependencies are dynamically linked.
    This makes multi-stage Docker builds difficult, as there is no easily accessible documentation of the linkages and      dependencies.
    While static building is [possible](http://docs.frrouting.org/projects/dev-guide/en/latest/static-linking.html), it is quite complicated, and we were not able to get it to work properly.
1.  All the build files and dependencies result in a large (1.5 GB) Docker image.
    Due to how the [Docker image file system works](https://stackoverflow.com/a/40216891), removing all files not needed to run FRR does not reduce the image size by itself.
    The image should therefore be built using `docker build --squash`, which removes all files not present in the last build layer, resulting in a much smaller image.
    Squashing is currently experimental, so you need to [enable it for the Docker daemon](https://thenewstack.io/how-to-enable-docker-experimental-features-and-encrypt-your-login-credentials/).
1. Library and executable paths are different, and need to either be adjusted in the build commands or in the dependent rettij methods.

This build path is implemented in `build.Dockerfile`.