#!/usr/bin/env bash

# Set up the rettij environment and install rettij.
# You can specify a path to a local dist file or specify a specific rettij version to be installed.
# usage: install-rettij.sh [-p <package>] [-v <venv_path>]

while getopts ":p:v:" opt; do
  case $opt in
    p) package="$OPTARG"
    ;;
    v) venv_path="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done


set -x  # print executed commands
set -e  # fail script once any command fails

# Install python
sudo apt-get update
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install -y python3.8 python3.8-venv

#Install k3s
sudo mkdir -p /etc/rancher/k3s
echo "
write-kubeconfig: /root/.kube/config
write-kubeconfig-mode: '0644'
disable:
  - 'traefik'
  - 'servicelb'
  - 'metrics-server'
" | sudo tee /etc/rancher/k3s/config.yaml
sudo curl -sfL https://get.k3s.io | sh -s -
mkdir -p ~/.kube && sudo ln /root/.kube/config ~/.kube/config

# Create venv and install rettij
venv_path="${venv_path:-./venv}"
python3.8 -m venv "$venv_path"
"$venv_path/bin/pip3" install wheel
"$venv_path/bin/pip3" install "${package:-rettij}"  # https://stackoverflow.com/a/9333006 -> Use 'rettij' if no package was provided

echo "Installed rettij to $venv_path (working directory: $PWD)"
